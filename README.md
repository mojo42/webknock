# Webknock

Makes a special URL execute a script

# Features

- Multiple URL with custom commands
- Optional command after a timeout
- Optional initialization command per URL
- Optional HTTP redirection
- Optional SSL support

# Initial need

The original idea is pretty similar to [port knocking](https://en.wikipedia.org/wiki/Port_knocking) but with web URLs.

The initial need was to be able to access a web application from anywhere without having to expose a potentially vulnerable application to the whole world.

Webknock allow you to visit a special URL and execute a local command to allow a visitor (e.g. run an iptables command).
Of course you can use this to perform other tasks.

## Example

An iptables based example is available in [config.yaml](./config.yaml).

- Webknock starts and block all connexions to server's port 443
- Webknock listens to port 8000
- A web client visits http://yourserver.com:8000/AllowMePlease
- Webknock allows client's IP to access server's port 443
- Webknock redirects web client to https://yourserver.com/
- Webknock will clear access to port 443 in 1 hour

# Building without SSL support

- Install [Rustlang](https://www.rust-lang.org/)
- Build with `cargo build --release`
- `webknock` binary is now available in `./target/release/`

# Building with SSL support

- Install [Rustlang](https://www.rust-lang.org/)
- Make sure pkg-config and development packages of openssl installed. For example, `libssl-dev` on Ubuntu or `openssl-devel` on Fedora.
- Build with `cargo build --release --features="ssl"`
- `webknock` binary is now available in `./target/release/`
- Setup `ssl_*` options in [config.yaml](./config.yaml)

# Running

- Customize `config.yaml` file
- Run `./webknock -c config.yaml`

# License

Webknock project is compliant with version 3.0 of the [REUSE](https://reuse.software/) Specification.

Source files are licensed under the [GNU Affero General Public License v3.0 or later](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).

