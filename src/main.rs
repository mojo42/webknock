/*
 * Webknock, makes a special URL execute a script
 * Copyright (C) 2019  Jérôme Jutteau <jerome@jutteau.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2019 Jérôme Jutteau <jerome@jutteau.fr>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

use clap;
use clap::{App, Arg};
use serde::Deserialize;
use serde_yaml;
use std::cmp::{Ordering, Reverse};
use std::collections::{BinaryHeap, HashMap};
use std::net::IpAddr;
use std::path::Path;
use std::process::{exit, Command};
use std::sync::{Arc, Mutex};
use std::thread::sleep;
use std::time::SystemTime;
use std::{error, fmt, fs, io, thread, time};
use tiny_http;
use tiny_http::{Server, ServerConfig, SslConfig};

const DEFAULT_CONFIG_PATH: &str = "/etc/webknock/config.yaml";

fn main() {
    let config = match Config::new() {
        Ok(c) => c,
        Err(e) => {
            eprintln!("{}", e);
            eprintln!("Use --help for more information");
            exit(1);
        }
    };
    if config.urls.len() == 0 {
        eprintln!("Error: no url configured.");
        exit(1);
    }
    initialize(&config);
    serve(&config);
}

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
struct Url {
    path: String,
    initialize: Option<String>,
    activate: String,
    timeout: Option<u64>,
    deactivate: Option<String>,
    redirect: Option<String>,
}

#[derive(Deserialize, Debug)]
struct Config {
    host: String,
    port: u16,
    ssl_certificate: Option<String>,
    ssl_private_key: Option<String>,
    urls: Vec<Url>,
}

impl Config {
    fn new() -> Result<Config, ConfigError> {
        let config_help = format!(
            "path to configuration file, default: {}",
            DEFAULT_CONFIG_PATH
        );
        let app = App::new("webknock")
            .version("1.0")
            .author("Jérôme Jutteau <jerome@jutteau.fr>")
            .about("Run action on specific URL")
            .arg(
                Arg::with_name("config")
                    .short("c")
                    .long("config")
                    .value_name("PATH")
                    .help(config_help.as_str())
                    .takes_value(true),
            );
        let cmd = app.get_matches();
        let config_path_str = cmd.value_of("config").unwrap_or(DEFAULT_CONFIG_PATH);
        let config_path = Path::new(config_path_str);
        let s = match fs::read_to_string(config_path) {
            Ok(s) => s,
            Err(e) => return Err(ConfigError::IoError(e)),
        };
        let config = match serde_yaml::from_str(&s) {
            Ok(c) => c,
            Err(e) => return Err(ConfigError::ParseError(e)),
        };
        Ok(config)
    }
}

#[derive(Debug)]
enum ConfigError {
    IoError(io::Error),
    ParseError(serde_yaml::Error),
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ConfigError::IoError(ref e) => f.write_fmt(format_args!(
                "Error while reading configuration file: {}",
                e
            )),
            ConfigError::ParseError(ref e) => f.write_fmt(format_args!(
                "Error while parsing configuraiton file: {}",
                e
            )),
        }
    }
}

impl error::Error for ConfigError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(self)
    }
}

fn initialize(config: &Config) {
    for url in &config.urls {
        if let Some(init) = &url.initialize {
            exec_cmd(&init, None);
        }
    }
}

fn ssl(config: &Config) -> Option<SslConfig> {
    if !cfg!(feature = "ssl") {
        return None;
    }
    if config.ssl_certificate.is_none() && config.ssl_private_key.is_none() {
        return None;
    }
    let cert_path = match &config.ssl_certificate {
        Some(c) => c,
        None => {
            eprintln!("Error while reading SSL configuration: ssl_certificate not set");
            exit(1);
        }
    };
    let secret_path = match &config.ssl_private_key {
        Some(s) => s,
        None => {
            eprintln!("Error while reading SSL configuration: ssl_private_key not set");
            exit(1);
        }
    };
    let cert_pem = match fs::read_to_string(cert_path) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Error while loading SSL certificate: {}", e);
            exit(1);
        }
    };
    let secret_pem = match fs::read_to_string(secret_path) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Error while loading SSL private key: {}", e);
            exit(1);
        }
    };
    Some(SslConfig {
        certificate: cert_pem.into_bytes(),
        private_key: secret_pem.into_bytes(),
    })
}

fn serve(config: &Config) {
    let mut knock = Knock::new(&config);
    let mut k = knock.clone();
    thread::spawn(move || k.auto_expire());

    let listen = format!("{}:{}", config.host, config.port);
    let serv_config = ServerConfig {
        addr: listen,
        ssl: ssl(&config),
    };
    let server = match Server::new(serv_config) {
        Ok(server) => {
            println!("Webknock now listing to {}:{}", config.host, config.port);
            server
        }
        Err(e) => {
            eprintln!("Error while starting server: {}", e);
            exit(1);
        }
    };
    for request in server.incoming_requests() {
        let url = request.url();
        let ip = request.remote_addr().ip();
        let response = knock.query(&url, &ip);
        request.respond(response).ok();
    }
}

type Response = tiny_http::Response<std::io::Cursor<std::vec::Vec<u8>>>;

#[derive(Debug, Clone)]
struct Knock {
    // path -> url config
    urls: HashMap<String, Url>,
    visitors: Arc<Mutex<BinaryHeap<Reverse<Visitor>>>>,
}

impl Knock {
    fn new(config: &Config) -> Knock {
        let mut urls = HashMap::<String, Url>::new();
        for url in config.urls.iter() {
            urls.insert(url.path.clone(), url.clone());
        }
        Knock {
            urls: urls,
            visitors: Arc::new(Mutex::new(BinaryHeap::new())),
        }
    }

    fn expire(&mut self) {
        let mut visitors = match self.visitors.lock() {
            Ok(v) => v,
            Err(_) => return,
        };
        while let Some(visitor) = visitors.peek() {
            if visitor.0.expire_date <= now() {
                if let Some(deactivate) = &visitor.0.url.deactivate {
                    exec_cmd(&deactivate, Some(&visitor.0.ip));
                }
                visitors.pop();
            } else {
                break;
            }
        }
    }

    fn auto_expire(&mut self) {
        let duration = time::Duration::from_secs(1);
        loop {
            sleep(duration);
            self.expire();
        }
    }

    fn query(&mut self, url: &str, ip: &IpAddr) -> Response {
        let mut response = Response::from_string("");
        if let Some(url) = self.urls.get(&String::from(url)) {
            exec_cmd(&url.activate, Some(&ip));
            if let Some(timeout) = url.timeout {
                let visitor = Visitor {
                    ip: ip.clone(),
                    expire_date: now() + timeout,
                    url: url.clone(),
                };
                if let Ok(mut v) = self.visitors.lock() {
                    v.push(Reverse(visitor));
                }
            }

            if let Some(redirect) = &url.redirect {
                response = Response::from_string("").with_status_code(307);
                if let Ok(header) =
                    tiny_http::Header::from_bytes(&b"Location"[..], redirect.as_str())
                {
                    response.add_header(header);
                }
            }
        }
        response
    }
}

#[derive(Eq, Debug)]
struct Visitor {
    ip: IpAddr,
    expire_date: u64,
    url: Url,
}

impl Ord for Visitor {
    fn cmp(&self, other: &Self) -> Ordering {
        self.expire_date.cmp(&other.expire_date)
    }
}

impl PartialOrd for Visitor {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Visitor {
    fn eq(&self, other: &Self) -> bool {
        self.expire_date == other.expire_date
    }
}

fn now() -> u64 {
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(v) => v.as_secs(),
        Err(e) => {
            eprintln!("Error while reading system time: {}", e);
            0
        }
    }
}

fn exec_cmd(cmd: &String, ip: Option<&IpAddr>) {
    let mut cmd = cmd.clone();
    if let Some(ip) = ip {
        cmd = cmd.replace("$IP", format!("{}", ip).as_str());
    }
    println!("exec: {}", cmd);
    let output = if cfg!(target_os = "windows") {
        Command::new("cmd").args(&["/C", cmd.as_str()]).output()
    } else {
        Command::new("sh").args(&["-c", cmd.as_str()]).output()
    };
    let output = match output {
        Ok(o) => o,
        Err(e) => {
            eprintln!("Error while executing command {}: {}", cmd, e);
            return;
        }
    };
    if let Ok(stdout) = String::from_utf8(output.stdout) {
        println!("stdout: {}", stdout);
    }
    if let Ok(stderr) = String::from_utf8(output.stderr) {
        println!("stderr: {}", stderr);
    }
}
